import ctypes, sys, time, os
import subprocess
import win32api, win32con

TINC_PATH = "C:\Program Files\\tinc"
TINC_EXE = "C:\\Program Files\\tinc\\tinc.exe"
TINC_VPN_PATH = "C:\\Program Files\\tinc\\vpn"

TAP_BAT = "C:\\Program Files\\tinc\\tap-win64\\tapinstall.exe"
TAP_DIR = "C:\\Program Files\\tinc\\tap-win64"

IP = "20.0.0.1"


def is_admin() -> bool:
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


def test_tinc_installed() -> bool:
    if not os.path.isdir(TINC_PATH):
        return False

    if not os.path.isfile(TINC_EXE):
        return False

    return True


def test_tinc_setup() -> bool:
    return os.path.isdir(TINC_VPN_PATH)


def start_process(command: [str]):
    cmd = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    try:
        print(cmd.stdout.read().decode("utf-8"))
        print(cmd.stderr.read().decode("utf-8"))
    except UnicodeDecodeError:
        pass


def start_process_cwd(command: [str], cwd: str):
    cmd = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd=cwd)
    print(cmd.stdout.read().decode("utf-8"))
    print(cmd.stderr.read().decode("utf-8"))



def initialize_tinc():
    print("Tinc wird initialisiert...")
    print("\n")
    print("\n")
    print("==== KONFIGURATION ====")
    name = input("Gebe einen Namen ein (ohne Leer- und Sonderzeichen): ")
    start_process([TINC_EXE, "-n", "vpn", "init", name])

    # Configuration via tinc.exe. Refer to https://www.tinc-vpn.org/documentation-1.1/tinc.conf.5
    start_process([TINC_EXE, "-n", "vpn", "add", "mode=switch"]) # notwendig fuer broadcasts und multicasts
    start_process([TINC_EXE, "-n", "vpn", "add", "digest=SHA512"]) # tinc benutzt standardmaessig sha1...
    start_process([TINC_EXE, "-n", "vpn", "add", "subnet=" + IP]) # tinc benutzt standardmaessig sha1...
    print("Konfiguration abgeschlossen")

def initialize_ethernet_interface():
    print("Das Ethernet-Interface wird nun eingerichtet...")
    print("Entferne alle installierten TAP-Treiber.")
    start_process_cwd([TAP_BAT, "remove", "tap0901"], cwd=TAP_DIR)

    print("Installiere TAP-Treiber.")
    start_process_cwd([TAP_BAT, "install", "OemWin2k.inf", "tap0901"], cwd=TAP_DIR)
    time.sleep(10) #workaround
    print("Richte neues Interface ein.")
    start_process(["netsh", "interface", "set", "interface", "name", "=", "Ethernet 2", "newname", "=", "tinc"])
    start_process(["netsh", "interface", "ip", "set", "address", "tinc", "static", IP, "255.255.255.0"])
    print("Einrichtung des Netzwerk-Interfaces abgeschlossen.")

def init_ethernet():
    print("ACHTUNG!")
    print("Im Folgenden werden Deine Netzwerkgeräte angezeigt.")
    print("Wähle mit gehaltener STRG-Taste Dein Internet-Gerät und das Gerät \"tinc\" aus.")
    print("Rechtsklicke dann eines der Geräte und wähle \"Verbindungen überbrücken\" aus.")
    print("Wechsele danach wieder auf dieses Fenster zurück.")
    input("Drücke ENTER, wenn Du bereit bist.")
    win32api.WinExec('control.exe ncpa.cpl', win32con.SW_NORMAL)
    input("Drücke jetzt ENTER, wenn Du fertig bist.")

def upload_file():
    print("Daten der anderen Eichler werden geladen.")
    print("Deine VPN-Daten werden nun hochgeladen.")

def main():
    if not test_tinc_installed():
        print("Tinc wurde nicht installiert.")
        input("Drücke irgendwas um das Fenster zu schließen.")
        exit(17)

    print("Die Tinc-Installation wurde erkannt.")
    print("\n")
    if not test_tinc_setup():
        initialize_tinc()
    else:
        print("Tinc-Konfiguration besteht bereits und wird daher übersprungen.")
    print("\n\n\n\n\n")
    initialize_ethernet_interface()
    print("\n\n\n\n\n")
    init_ethernet()

    print("Konfiguration abgeschlossen.")
    print("Verbinde Dich mit anderen Eichlern mit dem Befehl TODO.")

    input("Drücke irgendwas um das Fenster zu schließen.")

if is_admin(): #TODO
    main()
else:
    ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, "vpn_setup.py", None, 1)

