#! python3
import ctypes, sys, time, os
import subprocess
import ipgetter

TINC_PATH = "C:\Program Files\\tinc"
TINCD_EXE = "C:\\Program Files\\tinc\\tincd.exe"
TINC_EXE = "C:\\Program Files\\tinc\\tinc.exe"
TINC_VPN_PATH = "C:\\Program Files\\tinc\\vpn"


def test_tinc_installed() -> bool:
    if not os.path.isdir(TINC_PATH):
        return False

    if not os.path.isfile(TINCD_EXE):
        return False

    return True


def test_tinc_setup() -> bool:
    return os.path.isdir(TINC_VPN_PATH)


def is_admin() -> bool:
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


def start_process(command: [str]):
    cmd = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    try:
        print(cmd.stdout.read().decode("utf-8"))
        print(cmd.stderr.read().decode("utf-8"))
    except UnicodeDecodeError:
        pass


def update_config():
    myip = ipgetter.myip()
    print("Erkannte Public-IP: " + myip)
    start_process([TINC_EXE, "-n", "vpn", "add", "address=" + myip])

    pass #@TODO


def main():
    if not test_tinc_installed():
        print("Tinc wurde noch nicht installiert. Installiere zunächst Tinc 1.1.\n")
        print("Schaue Dir https://www.tinc-vpn.org/download/ an.")

        input("Drücke irgendwas.")
        exit(1)

    if not test_tinc_setup():
        print("Du hast das Konfigurationstool noch nicht gestartet.")

        input("Drücke irgendwas.")
        exit(2)

    update_config()

    print("Alle Tests waren erfolgreich.")
    print("Tinc wird jetzt gestartet")
    start_process([TINCD_EXE, "-n", "vpn", "-D", "-d3"])
    input("Fertig.")

if is_admin():
    main()
else:
    ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, "vpn_start.py", None, 1)

